class OrdersController < ApplicationController
  skip_before_action :verify_authenticity_token  
  
  def index
    @orders = Order.where(:user_id => current_user.id)
  end
  
  def show
    @shipping_address = current_user.shipping_addresses.all
    @order = Order.create(:user_id => current_user.id, :shipping_addresses_id => @shipping_address.first.id)
    if @order.save 
      OrderMailer.with(order: @order).new_order_email.deliver_later
      flash[:success] = "Thank you for your order! We'll get contact you soon!"
    else
      flash.now[:error] = "Your order form had some errors. Please check the form and resubmit."
      render :new[]
    end
    @current_cart.cart_items.each do |item|
      @order_item = OrderItem.create(:product_id => item.product_id, :order_id => @order.id, :product_cost => item.product.price , :quantity => item.quantity,:total_cost =>item.total_price)
      @order_item.save
      item.cart_id = nil
    end
    Cart.destroy(session[:cart_id])
    session[:cart_id] = nil
  end

  def new
    @order = Order.new
  end
end
