class ApplicationController < ActionController::Base
  add_flash_types :info, :error, :warning
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  before_action :current_cart
  protect_from_forgery with: :exception

  before_action :current_cart
  protected

    def configure_permitted_parameters
      #devise_parameter_sanitizer.for(:sign_up) << :name
      #devise_parameter_sanitizer.for(:account_update) << :name
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :role, :password,:avatar])
      #devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    end

    def after_sign_up_path_for(resource)
      user_path(resource)
    end

    #def after_sign_in_path_for(resource)
     # user_path(resource)
    #end
  
  private
    def current_cart
      if session[:cart_id]
        cart = Cart.find_by(:id => session[:cart_id])
        if cart.present?
          @current_cart = cart
        else
          session[:cart_id] = nil
        end
      end
      if session[:cart_id] == nil
        if current_user    
          cart = Cart.find_by(:user_id => current_user.id)
          if cart.present?
            @current_cart = cart
          else
            @current_cart = Cart.create(user_id: current_user.id)
            @current_cart.save
          end
          session[:cart_id] = @current_cart.id
        end
      end
    end  
end

