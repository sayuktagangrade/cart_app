class PagesController < ApplicationController
  def search
    if params[:search].blank?
      redirect_to(root_path, alert: "Empty field!") and return  
    else
      @parameter = params[:search].downcase  
      @results = Product.all.where("lower(product_name) LIKE :search", search: @parameter)
      if @results.nil?
        redirect_to(root_path, alert: "no items match according to your search") and return
      end
    end
  end
end
