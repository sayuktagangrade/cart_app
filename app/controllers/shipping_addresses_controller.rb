class ShippingAddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update, :destroy]
  def new
    @shipping_address = ShippingAddress.new
  end
  def index
    @shipping_address = ShippingAddress.all
  end
  def create
    @shipping_address = current_user.shipping_addresses.create(:pincode => address_params[:pincode],:address => address_params[:address],:locality => address_params[:locality],:save_address_as => address_params[:save_address_as])
    if @shipping_address.valid?
      @shipping_address.save
      render 'orders/index'
    else
      flash[:notice] = "You have not properly fill all the coloumns"
      render 'shipping_addresses/new'
    end
  end
  def edit
    
  end
  def destroy
    @shipping_address.destroy
    flash[:notice] = "Address was successfully destroyed." 
    redirect_to orders_path
  end
  def update
    if @shipping_address.update(address_params)
      redirect_to orders_path
    else    
      render 'edit'
    end
  end

private
  
  def set_address
    @shipping_address = ShippingAddress.find(params[:id])
  end

  def address_params
    params.require(:shipping_address).permit(:pincode, :address, :locality, :save_address_as)
  end
end
