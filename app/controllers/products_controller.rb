class ProductsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_product, only: [:edit , :destroy , :update]
  

  def new
    @product = Product.new
  end

  def index
    @products = current_user.products
    respond_to do |format|
      format.html
      format.csv { send_data @products.to_csv }
    end
    
  end

  def create
    @product = current_user.products.create(products_params)

    if @product.valid?
      @product.save
      redirect_to :root
    else
      flash[:notice] = "You have not properly fill all the coloumns"
      render 'products/new'
    end
  end

  def edit
  end

  def destroy
    @product.destroy
    redirect_to root_path
  end

  def update
       
    if @product.update(products_params)
      redirect_to products_path
    else
      render 'edit'
    end
  end
  
  def products_params
    params.require(:product).permit(:product_name, :description, :price, :image)
  end

  def set_product
    @product = Product.find(params[:id]) 
  end
 
end