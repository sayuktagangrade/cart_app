class RegistrationController < ApplicationController

  def index
    # @registration = Registration.find(params[:id])
  end

  def signup

  end
  
  def login
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      esession[:user_id] = user.id
      flash[:success] = ‘Successfully Logged In!’
      redirect_to :root
    else
      flash[:notice] = "Invalid Usrname or Password"
      redirect_to :registration_index
    end
  end

  def create
  
    @user = User.new(user_params)

    if @user.valid?
      @user.save
      session[:user_id] = @user.id
      flash[:notice] = 'Welcome.'
      redirect_to :root
    else
      flash[:notice] = "You have not properly fill all the coloumns"
      render 'signup'
    end
    
  end

  def new
    @user = User.new
  end

  def user_params
    params.permit(:name, :email, :password, :role)
  end
end
