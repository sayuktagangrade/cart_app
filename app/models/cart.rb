class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_items, dependent: :destroy
  has_many :products
  validates :user_id, uniqueness: true
  def sub_total
    sum = 0
    self.cart_items.each do |cart_item|
      sum+= cart_item.total_price
    end
    return sum
  end
end
