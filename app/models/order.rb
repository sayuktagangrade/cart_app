class Order < ApplicationRecord
 belongs_to :user
 belongs_to :location, optional: true
 has_many :order_items
end
