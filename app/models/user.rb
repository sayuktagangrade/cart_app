class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, presence: true
  has_many :products
  has_many :shipping_addresses
  has_one :cart  
  has_one_attached :avatar
  after_create :send_mail
  def send_mail
    UserMailer.send_enabled_message(self).deliver
  end
end
