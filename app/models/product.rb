class Product < ApplicationRecord
  # belongs_to :product_category
  
  validates :price, :description, :product_name, presence: true
  belongs_to :user
  has_many :cart_items, dependent: :destroy
  has_one_attached :image
  def self.to_csv
    attributes = %w{id price description product_name}
      CSV.generate(headers: true) do |csv|
        csv << attributes
  
        all.find_each do |product|
          csv << attributes.map{ |attr| product.send(attr) }
        end
      end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      product = find_by_id(row["id"]) || new
      product.attributes = row.to_hash.slice(*accessible_attributes)
      product.save!
    end
  end
  
  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when '.csv' then Roo::Csv.new(file.path, nil, :ignore) 
    when ".xls" then Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
  
   
end
