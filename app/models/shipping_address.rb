class ShippingAddress < ApplicationRecord
   validates :pincode, :address, :locality, :save_address_as, presence: true
   validates :pincode, length: { is: 6 }
   belongs_to :user
end
