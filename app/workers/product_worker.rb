class ProductAddWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
  skip_before_action :verify_authenticity_token
  before_action :set_product, only: [:edit , :destroy , :update]
 

  def perform
    @product = Product.new(products_params)
    if @product.valid?
      @product.save
      redirect_to :root
    else
      flash[:notice] = "You have not properly fill all the coloumns"
      render 'products/new'
    end
  end

 def products_params
    params.require(:product).permit(:product_name, :description, :price, :image)
  end
  def set_product
    @product = Product.find(params[:id]) 
  end

end

class ProductRemoveWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
  skip_before_action :verify_authenticity_token
  before_action :set_product, only: [:edit , :destroy , :update]
  def perform
    @product.destroy
    redirect_to root_path
  end

  
  def products_params
    params.require(:product).permit(:product_name, :description, :price, :image)
  end
  def set_product
    @product = Product.find(params[:id]) 
  end
end