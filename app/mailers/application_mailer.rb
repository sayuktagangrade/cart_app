class ApplicationMailer < ActionMailer::Base
  default from: 'sayuktagangrade@gmail.com'
  layout 'mailer'
end
