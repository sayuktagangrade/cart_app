class OrderMailer < ApplicationMailer
  def new_order_email
    @order = params[:order]
    @user = User.find(@order.user_id)
    mail(to: @user.email, subject: "You got a new order!")
  end
end
