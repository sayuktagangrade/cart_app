class UserMailer < ApplicationMailer
  default from: "sayuktagangrade@gmail.com"
  def send_enabled_message(user)
    @user = user
    mail(to: @user.email, subject: "welcome to our shopping site")
  end
end
