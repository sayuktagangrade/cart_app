# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
product.create(price: 20)
Product.create(price: 500,description: "JBL unisex blue flip 4 protable bluetooth speaker",product_name: 'speaker')
Product.create(price: 1000,description: "boat rockerz 255 active black in ear wireless earphones",product_name: 'earphone')
Product.create(price: 1000,description: "men regular fit polo tshirt",product_name: 'Tees')
Product.create(price: 1000,description: "Solimo Swirl 1200mm Ceiling Fan (Brown) ",product_name: 'Fan')
Product.create(price: 1999,description: "Dualshock 4 Wireless Controller for Playstation 4 - Black V2  ",product_name: 'Controller') 
Product.create(price: 999,description: "The Power of your Subconscious Mind ",product_name: 'Book')
Product.create(price: 1499,description: "Yonex Muscle Power 29 Badminton Racquet (G4 - 88g)",product_name: 'Badminton')
Product.create(price: 250,description: "Mansaa Essentials 100 LED Copper Fairy String 10 m Lights with 1 m USB Cable for Home Decoration, Party , Warm White ",product_name: 'Lights')
Product.create(price: 25000,description: "Canon EOS 1500D 24.1MP Digital SLR Camera (Black) with 18-55 and 55-250mm is II Lens, 16GB Card and Carry Case  ",product_name: 'Camera')
 Product.create(price: 2500,description: "Mi Smart Band 4- India's No.1 Fitness Band, Up-to 20 Days Battery Life, Color AMOLED Full-Touch Screen, Waterproof with Music Control and Unlimited Watch",product_name: 'Watch')
