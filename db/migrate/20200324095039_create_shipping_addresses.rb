class CreateShippingAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :shipping_addresses do |t|
      t.string :pincode
      t.text :address
      t.text :locality
      t.string :save_address_as

      t.timestamps
    end
  end
end
