class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.float :price
      t.text :description
      t.string :product_name

      t.timestamps
    end
  end
end
