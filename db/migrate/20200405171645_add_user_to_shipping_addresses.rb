class AddUserToShippingAddresses < ActiveRecord::Migration[6.0]
  def change
    add_reference :shipping_addresses, :user, index: true
  end
end
