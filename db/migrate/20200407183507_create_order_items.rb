class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_join_table :products, :orders, table_name: :order_items do |t|
      t.index :product_id
      t.index :order_id
      t.float :product_cost
      t.integer :quantity
      t.float :total_cost
      t.timestamps
    end
  end
end
