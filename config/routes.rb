Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  resources :notifications
  resources :comments
  get 'events/index'
  get 'orders/index'
  get 'orders/show'
  get 'orders/new'
  devise_for :users
  mount ActionCable.server => '/cable'
  #devise_for :user, controllers: { sessions: 'users/sessions' }
  #get 'dashboard/index'
  #get '/registration/index', to: 'registration#index'
  #get 'signup' => 'registration#signup'
  #get 'new' => 'registration#new'
  #post 'login' => 'registration#login'
  
  #post "create"  => "registration#create"
  #devise_scope :user do
   # root to: 'devise/sessions#new'
  #end
  #devise_scope :users do
   # get '/users/sign_out' => 'devise/sessions#destroy'
  #end
  #get 'users(.:format)' => 'devise/registrations#show'
  get '/search' => 'pages#search', :as => 'search_page'
  #get 'carts/:id' => "carts#show", as: "cart"
  #delete 'carts/:id' => "carts#destroy"
  resources :carts
  resources :cart_items
  post 'cart_items/:id/add' => "cart_items#add_quantity", as: "cart_item_add"
  post 'cart_items/:id/reduce' => "cart_items#reduce_quantity", as: "cart_item_reduce"
  
  #post 'cart_items' => "cart_items#create"
  #get 'cart_items/:id' => "cart_items#show", as: "cart_item"
  #delete 'cart_items/:id' => "cart_items#destroy"
  resources :products do
    collection { post :import }
  end
  get '/secret', to: 'pages#secret', as: :secret
  root 'dashboard#index'
  resources :orders
  get 'notification/index'
  resources :order_items
  #post 'order_items' => "order_items#create"
  #get 'order_items' => "order_items#index", as: "order_item"
  resources :shipping_addresses
end
